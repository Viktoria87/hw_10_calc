const calc = {
    operand1: "",
    operand2: "",
    sign: "", 
    rez: ""
};



window.addEventListener("DOMContentLoaded", () => {
    const btns = document.querySelector(".keys");
    btns.addEventListener("click", click)
})

const click = (e) => {
    //console.log(e.target.value)
    if (/[0-9.]/.test(e.target.value) && calc.sign === "") {
        calc.operand1 += e.target.value
        show(calc.operand1)
    } else if (/[0-9.]/.test(e.target.value) && calc.sign !== "") {
        calc.operand2 += e.target.value
        show(calc.operand2)
    } else if (/[+*-/]/.test(e.target.value)) {
        calc.sign = e.target.value
        show(calc.sign)
    } else if (/[=]/.test(e.target.value)){
        calc.rez = operation(calc.operand1, calc.operand2, calc.sign)
        document.querySelector(".display > input").value.innerHTML = calc.rez
        show(calc.rez)
        
    } else if (/[C]/.test(e.target.value)){
        clear();
    } else {
        console.error("error")
    }
}

const show = (data) => {
    document.querySelector(".display > input").value = data
}

//мои пробы

const operation = (n1, n2, sign) => {
    switch(sign){
        case "+" : return parseFloat(n1) + parseFloat(n2);
        case "-" : return n1 - n2;
        case "*" : return n1 * n2;
        case "/" : return n1 / n2;
    };
};

const clear = () => {
    calc.operand1 = "";
    calc.operand2 = "";
    calc.sign = "";
    document.querySelector(".display > input").value = "";
    
   
};
